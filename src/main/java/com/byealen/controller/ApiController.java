package com.byealen.controller;

import com.byealen.entity.R;
import com.byealen.entity.Result;
import com.byealen.util.JrebelSign;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

@RestController
public class ApiController {

    @PostMapping(value = "info")
    public R<Map<Object, Object>> web(HttpServletRequest request) {
        String basePath = request.getScheme() + "://" + request.getServerName();
        if (request.getServerPort() != 80) {
            basePath += ":" + request.getServerPort();
        }
        HashMap<Object, Object> map = new HashMap<>(2);
        map.put("url", basePath);
        map.put("key", UUID.randomUUID().toString());
        return R.data(map);
    }

    @RequestMapping(value = {"/jrebel/leases", "/agent/leases"})
    public Result jrebelLeasesHandler(
            HttpServletResponse response,
            @RequestParam("randomness") String clientRandomness, @RequestParam(value = "username", required = false) String username,
            @RequestParam("guid") String guid, @RequestParam(value = "offline", required = false) boolean offline,
            @RequestParam("clientTime") Long clientTime) {
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        Long validFrom = null;
        Long validUntil = null;
        if (offline & clientTime != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(clientTime);
            cal.add(Calendar.MONTH, 6);
            long clinetTimeUntil = cal.getTimeInMillis();
            validFrom = clientTime;
            validUntil = clinetTimeUntil;
        }

        Result result = new Result();
        result.setServerVersion("3.2.4");
        result.setServerProtocolVersion("1.1");
        result.setServerGuid("a1b4aea8-b031-4302-b602-670a990272cb");
        result.setGroupType("managed");
        result.setId(1);
        result.setLicenseType(1);
        result.setEvaluationLicense(false);
        result.setSignature("OJE9wGg2xncSb+VgnYT+9HGCFaLOk28tneMFhCbpVMKoC/Iq4LuaDKPirBjG4o394/UjCDGgTBpIrzcXNPdVxVr8PnQzpy7ZSToGO8wv/KIWZT9/ba7bDbA8/RZ4B37YkCeXhjaixpmoyz/CIZMnei4q7oWR7DYUOlOcEWDQhiY=");
        result.setSeatPoolType("standalone");
        result.setStatusCode("SUCCESS");
        result.setOffline(offline);
        result.setValidFrom(validFrom);
        result.setValidUntil(validUntil);
        result.setCompany("Administrator");
        result.setOrderId("");
        result.setZeroIds(new ArrayList<>());

        Calendar cal = Calendar.getInstance();
        result.setLicenseValidFrom(cal.getTimeInMillis());
        cal.add(Calendar.YEAR, 1);
        result.setLicenseValidUntil(cal.getTimeInMillis());

        if (clientRandomness == null || username == null || guid == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            JrebelSign jrebelSign = new JrebelSign();
            jrebelSign.toLeaseCreateJson(clientRandomness, guid, offline, validFrom, validUntil);
            result.setSignature(jrebelSign.getSignature());
            result.setServerRandomness(jrebelSign.getServerRandomness());
            result.setCompany(username);
            return result;
        }
        return null;
    }

    @RequestMapping(value = {"/jrebel/leases/{code}", "/agent/leases/{code}"})
    public Result jrebelLeases1Handler(HttpServletResponse response, @RequestParam("username") String username, @PathVariable("code") String code) {
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        Result result = new Result();
        result.setServerVersion("3.2.4");
        result.setServerProtocolVersion("1.1");
        result.setServerGuid("a1b4aea8-b031-4302-b602-670a990272cb");
        result.setGroupType("managed");
        result.setStatusCode("SUCCESS");
        result.setMsg(null);
        result.setStatusMessage(null);
        if (username != null) {
            result.setCompany(username);
        }
        return result;
    }

    @RequestMapping(value = "/jrebel/validate-connection")
    public Result jrebelValidateHandler(HttpServletResponse response, @RequestParam("username") String username) {
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        Result result = new Result();
        result.setServerVersion("3.2.4");
        result.setServerProtocolVersion("1.1");
        result.setServerGuid("a1b4aea8-b031-4302-b602-670a990272cb");
        result.setGroupType("managed");
        result.setStatusCode("SUCCESS");
        result.setCompany(username);
        result.setCanGetLease(true);
        result.setLicenseType(1);
        result.setEvaluationLicense(false);
        result.setSeatPoolType("standalone");

        return result;
    }
}
