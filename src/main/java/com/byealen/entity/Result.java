package com.byealen.entity;

import java.util.List;

public class Result {

    /**
     * serverVersion : 3.2.4
     * serverProtocolVersion : 1.1
     * serverGuid : a1b4aea8-b031-4302-b602-670a990272cb
     * groupType : managed
     * id : 1
     * licenseType : 1
     * evaluationLicense : false
     * signature : OJE9wGg2xncSb+VgnYT+9HGCFaLOk28tneMFhCbpVMKoC/Iq4LuaDKPirBjG4o394/UjCDGgTBpIrzcXNPdVxVr8PnQzpy7ZSToGO8wv/KIWZT9/ba7bDbA8/RZ4B37YkCeXhjaixpmoyz/CIZMnei4q7oWR7DYUOlOcEWDQhiY=
     * serverRandomness : H2ulzLlh7E0=
     * seatPoolType : standalone
     * statusCode : SUCCESS
     * offline : false
     * validFrom : null
     * validUntil : null
     * company : Administrator
     * orderId :
     * zeroIds : []
     * licenseValidFrom : 1490544001000
     * licenseValidUntil : 1691839999000
     */

    private String serverVersion;
    private String serverProtocolVersion;
    private String serverGuid;
    private String groupType;
    private Integer id;
    private Integer licenseType;
    private boolean evaluationLicense;
    private String signature;
    private String serverRandomness;
    private String seatPoolType;
    private String statusCode;
    private boolean offline;
    private Long validFrom;
    private Long validUntil;
    private String company;
    private String orderId;
    private Long licenseValidFrom;
    private Long licenseValidUntil;
    private List<?> zeroIds;

    private String msg;
    private String statusMessage;

    private boolean canGetLease;

    public String getServerVersion() {
        return serverVersion;
    }

    public void setServerVersion(String serverVersion) {
        this.serverVersion = serverVersion;
    }

    public String getServerProtocolVersion() {
        return serverProtocolVersion;
    }

    public void setServerProtocolVersion(String serverProtocolVersion) {
        this.serverProtocolVersion = serverProtocolVersion;
    }

    public String getServerGuid() {
        return serverGuid;
    }

    public void setServerGuid(String serverGuid) {
        this.serverGuid = serverGuid;
    }

    public String getGroupType() {
        return groupType;
    }

    public void setGroupType(String groupType) {
        this.groupType = groupType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLicenseType() {
        return licenseType;
    }

    public void setLicenseType(Integer licenseType) {
        this.licenseType = licenseType;
    }

    public boolean isEvaluationLicense() {
        return evaluationLicense;
    }

    public void setEvaluationLicense(boolean evaluationLicense) {
        this.evaluationLicense = evaluationLicense;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getServerRandomness() {
        return serverRandomness;
    }

    public void setServerRandomness(String serverRandomness) {
        this.serverRandomness = serverRandomness;
    }

    public String getSeatPoolType() {
        return seatPoolType;
    }

    public void setSeatPoolType(String seatPoolType) {
        this.seatPoolType = seatPoolType;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public boolean isOffline() {
        return offline;
    }

    public void setOffline(boolean offline) {
        this.offline = offline;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Long validUntil) {
        this.validUntil = validUntil;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getLicenseValidFrom() {
        return licenseValidFrom;
    }

    public void setLicenseValidFrom(Long licenseValidFrom) {
        this.licenseValidFrom = licenseValidFrom;
    }

    public Long getLicenseValidUntil() {
        return licenseValidUntil;
    }

    public void setLicenseValidUntil(Long licenseValidUntil) {
        this.licenseValidUntil = licenseValidUntil;
    }

    public List<?> getZeroIds() {
        return zeroIds;
    }

    public void setZeroIds(List<?> zeroIds) {
        this.zeroIds = zeroIds;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public boolean isCanGetLease() {
        return canGetLease;
    }

    public void setCanGetLease(boolean canGetLease) {
        this.canGetLease = canGetLease;
    }
}