package com.byealen.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LogVO {

    // ip
    private String ip;

    // url
    private String url;

    // 请求方式 GET POST
    private String httpMethod;

    // 类方法
    private String classMethod;

    // 请求参数
    private Object requestParams;

    // 返回参数
    private Object result;

    // 接口耗时
    private Long timeCost;

}
